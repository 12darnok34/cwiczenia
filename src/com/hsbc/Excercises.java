package com.hsbc;

//psvm = public static void main

import com.hsbc.calc.Calculator;

public class Excercises {

    public static void main(String[] args) {

        int x = args.length > 0 ? Integer.parseInt(args[0]) : -1;

        int[] intArray = new int[]{5, 3, 7, 2, 1, 9, 0, 6, 4, x};


        createMultiplicationTable(10);

    }

    public static void printArray(int[] array){

            for(int index=0;index<array.length;index++){

                System.out.println(array[index]);


        }
    }

    public static void printArrayUsingWhile(int[] x) {

        int index = 0;

        while(index<x.length){

            System.out.println(x[index]);

            index++;

        }

    }

    public static void printLastThreeFromArray(int[] x){

        int index = x.length>=3 ? x.length-3 : 0;

        while(index<x.length){

            System.out.println(x[index] + "[" + index + "]");

            index++;

        }

    }

    public static void createMultiplicationTable(int size){

        int[][] values = new int[size][size];

        for(int x=1;x<=size;x++){

            for(int y=1;y<=size;y++){

                values[x-1][y-1] = (x*y);

                System.out.print(values[x-1][y-1] + "\t" );

            }

            System.out.println();

        }

        System.out.println("done.");



        /*for(int[] row : values){

           for(int value : row) {

               System.out.print(value + "\t" );

           }

           System.out.println();

        }*/

    }



}
