package com.hsbc;

import java.util.Scanner;

public class Test4 {


    public static void speed(double meters, double seconds) {
        double speed = (meters / 1000) / (seconds / 3600);

        System.out.println("speed in m/s=" + meters / seconds + ", km/h: =" + speed);

    }

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        System.out.print("\tplease enter distance in meters: ");

        double m = scanner.nextDouble();

        System.out.println();

        System.out.print("\n\tplease enter time in seconds: ");

        double s = scanner.nextDouble();

        speed(m, s);
    }

}